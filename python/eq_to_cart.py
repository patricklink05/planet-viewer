from astropy import coordinates as coord
from astropy import units as u
import sys
import json

def read_in():
    out = {}
    counter = 0
    for line in sys.stdin:
        try:
            boy = json.loads(json.loads(line))
        except:
            out[counter] = json.dumps({ "x" : x,
                     "y" : y,
                     "z" : z
                     }
                    )
            counter += 1
        out[counter] = equatorial_to_cartesian(boy["ra_str"], float(boy["dec"]), float(boy["st_dist"]))
        counter += 1
    return json.dumps(out)
    #line = sys.stdin[-1]
    #boy = json.loads(json.loads(line))
    #dict = {"type" : str(type(boy["ra_str"]))}
        #return json.dumps({
        #    "ra_str" : boy["ra_str"],
        #    "dec"    : boy["dec"],
        #    "st_dist": boy["st_dist"]
        #})
    #return json.dumps(equatorial_to_cartesian(boy["ra_str"], float(boy["dec"]), float(boy["st_dist"])))

def equatorial_to_cartesian(ra, dec, st_dist):
    """ Use Astropys SkyCoord to convert data for ra, dec, dist to x, y, z using
    the plane of the galaxy as the origin.
    """
    if not ra or not dec or not st_dist:
        return json.dumps({ "x" : 0,
                 "y" : 0,
                 "z" : 0
                 }
                )
    x = coord.SkyCoord(ra=ra,
                       dec=dec * u.degree,
                       distance=coord.Distance(st_dist,
                                               u.parsec)).galactic.cartesian.x.value
    y = coord.SkyCoord(ra=ra,
                       dec=dec * u.degree,
                       distance=coord.Distance(st_dist,
                                               u.parsec)).galactic.cartesian.y.value
    z = coord.SkyCoord(ra=ra,
                       dec=dec * u.degree,
                       distance=coord.Distance(st_dist,
                                               u.parsec)).galactic.cartesian.z.value
    # Insert the x, y, z coordinate series into the dataframe in the first three
    # columns
    return json.dumps({ "x" : x,
             "y" : y,
             "z" : z
             }
            )

if __name__ == "__main__":
    #print(equatorial_to_cartesian("18h49m50.53s",48.257118,465.53))
    print(read_in())
    sys.stdout.flush()
    #print(equatorial_to_cartesian(sys.argv[1],float(sys.argv[2]),float(sys.argv[3])))
