import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Exercise = props => (
  <tr>
    <td>{props.planet.pl_hostname}</td>
    <td>{props.planet.pl_letter}</td>
    <td>{props.planet.st_dist}</td>
    <td>{props.planet.dec}</td>
    <td>{props.planet.ra_str}</td>
    <td>{props.planet.x}</td>
    <td>{props.planet.y}</td>
    <td>{props.planet.z}</td>
    <td><a href="#" onClick={() => { props.deletePlanet() }}>delete</a></td>
  </tr>
)

export default class ExercisesList extends Component {
  constructor(props) {
    super(props);

    this.deletePlanet = this.deletePlanet.bind(this);

    this.state = {planet: []};
  }

  componentDidMount() {
    axios.get('http://localhost:5000/planet/')
      .then(response => {
        this.setState({ planet: response.data });
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deletePlanet() {
    axios.delete('http://localhost:5000/planet/deleteTop')
      .then(res => console.log(res.data));
    console.log(this.state.planet.shift())
    this.setState({
      planet: this.state.planet
    })
  }

  exerciseList() {
    return this.state.planet.map(currentexercise => {
      return <Exercise key={currentexercise._id} planet={currentexercise} deletePlanet={this.deletePlanet}/>;
    })
  }
  render() {
    return (
      <div>
        <h3>Discovered Exoplanets</h3>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th>Planet Host Name</th>
              <th>Planet Letter</th>
              <th>Stellar Distance</th>
              <th>Right Ascension</th>
              <th>Declination</th>
              <th>X</th>
              <th>Y</th>
              <th>Z</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            { this.exerciseList() }
          </tbody>
        </table>
      </div>
    )
  }
}
