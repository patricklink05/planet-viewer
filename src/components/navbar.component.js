import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Planet Viewer</Link>
        <div className="collpase navbar-collapse">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
          <Link to="/" className="nav-link">Planets</Link>
          </li>
          <li className="navbar-item">
          <Link to="/user" className="nav-link">Database Commands</Link>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
}
