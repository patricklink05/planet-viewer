import React, { Component } from 'react';
import axios from 'axios';
import delay from 'delay';

function chunkArrayInGroups(arr, size) {
  var myArray = [];
  for(var i = 0; i < arr.length; i += size) {
    myArray.push(arr.slice(i, i+size));
  }
return myArray;
};

export default class CreateUser extends Component {
  constructor(props) {
    super(props);

    this.onChangeUsername = this.onChangeUsername.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      planet: [],
    }
  }

  onChangeUsername(e) {
    this.setState({
      username: e.target.value
    });
  }

async onSubmit(e) {
    e.preventDefault();
    axios.get("https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets&select=pl_hostname,pl_letter,st_dist,dec,ra_str,pl_discmethod&format=json")
      .then(async function(res) {
        console.log(res)
        res.data = chunkArrayInGroups(res.data, 600)
        console.log(res.data)
        res.data.forEach(async (item) => {
          axios.post('http://localhost:5000/planet/addBulk',item)
          .then(() => console.log("boi"))
          .catch((err) => console.log("Error: " + err))
        })
      })
      .catch((error) => console.log(error));
    //window.location = '/'
  }

  onDeleteAll(e) {
    e.preventDefault();
    axios.delete('http://localhost:5000/planet/deleteAll')
    .then(res => console.log(res.data));
    //window.location = '/'
  }

  onConvert(e) {
    e.preventDefault();
    console.log("go")
    axios.get('http://localhost:5000/planet/convert')
    .then(res => console.log(res.data))
    //window.location = '/'
  }



/*
  onSubmit(e) {
    e.preventDefault();
    const user = {
      username: this.state.username,
    }

    console.log(user);

    axios.post('http://localhost:5000/users/add', user)
      .then(res => console.log(res.data));

    this.setState({
      username: ''
    })
  } */

  render() {
    return (
      <div>
        <h3>Load Planets</h3>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <input type="submit" value="Load Planets" className="btn btn-primary" />
          </div>
        </form>
        <h3>Delete All Planets</h3>
        <form onSubmit={this.onDeleteAll}>
          <div className="form-group">
            <input type="submit" value="Delete All Planets" className="btn btn-primary" />
          </div>
        </form>
        <h3>Convert Planets</h3>
        <form onSubmit={this.onConvert}>
          <div className="form-group">
            <input type="submit" value="Convert All Planets" className="btn btn-primary" />
          </div>
        </form>

      </div>
    )
  }
}
