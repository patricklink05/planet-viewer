const router = require('express').Router();
let Planet = require('../models/planet.model');
const { PythonShell } = require('python-shell');
const JSON = require('JSON');

/*
let runPy = new Promise(function(success, nosuccess) {

    const { spawn } = require('child_process');
    const pyprog = spawn('python3', ['/Users/patricklink/websites/mern-exercise-tracker/python/helloworld.py']);

    pyprog.stdout.on('data', function(data) {

        success(data);
    });

    pyprog.stderr.on('data', (data) => {

        nosuccess(data);
    });
});*/
function convertToJSON(planet) {
  return (

    JSON.stringify({
      ra_str: planet.ra_str,
      dec: planet.dec,
      st_dist: planet.st_dist
    }
  )
  );
};

router.route('/').get((req, res) => {
  Planet.find()
    .then(planets => {
      res.json(planets)})
    .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/findTop/').get((req, res) => {
  console.log("Hit")
  Planet.find()
    .then(planet => {
      console.log(planet.length)
      res.json(planet)
    })
    .catch(err => res.status(400).json("Error: " + err))
});

router.route('/add').post((req, res) => {

  const newPlanet = new Planet({
    pl_hostname : req.body.pl_hostname,
    pl_letter : req.body.pl_letter,
    st_dist : req.body.st_dist,
    dec : req.body.dec,
    ra_str : req.body.ra_str
  });

  newPlanet.save()
    .then(() => res.json('Planet added!'))
    .catch(err => res.status(400).json('Error: ' + err));
});



router.route('/convert/').get(function(req, res) {
  Planet.find()
  .then((planets) => {
    let start = 1
    let finish = planets.length
    var request = planets.slice(start,finish);
    let len = request.length
    request = request.map(convertToJSON);
    let out
    //console.log(request)
    const pyShell = new PythonShell('eq_to_cart.py',
                    {
                      mode: 'json',
                      pythonPath: '/usr/local/bin/python3',
                      scriptPath: '/Users/patricklink/websites/mern-exercise-tracker/python/'
                    });
    for (let i = 0; i < finish-start-1; i++) {
      let temp = JSON.parse(request[i])
        if (temp.st_dist == null) {
          console.log("bop")
          temp.st_dist = 0.0
          request[i] = JSON.stringify(temp)
        }
        console.log("Request",i,request[i])
        pyShell.send(request[i], { mode: 'json'});
    }
    request = planets.slice(start,finish)
    pyShell.on('message', results => {
      console.log(results);
      out = results
      let cords
      console.log("Length", out.length)
      for (let j = 0; j < finish-start-1;j++) {
        try {
          cords = JSON.parse(out[j.toString()])
        }
        catch (err) {
          console.log("Error : " + err)
          continue
        }

        request[j].x = cords.x;
        request[j].y = cords.y;
        request[j].z = cords.z
        console.log(cords.x," ",cords.y," ",cords.z)
        console.log(typeof request[j])
        request[j].save()
        .then(console.log("Done"))
        .catch((err) => console.log("Error: "+ err))

      }
      //out = JSON.parse(results)
    });

    //res.send(out);

    pyShell.end(err => {
      if (err) res.send("Error: ", err);
    });








    /*PythonShell.run('eq_to_cart.py',
                    {
                      mode: 'json',
                      pythonPath: '/usr/local/bin/python3',
                      scriptPath: '/Users/patricklink/websites/mern-exercise-tracker/python/',
                      args: request},//[req.body.ra_str,req.body.dec,req.body.st_dist]},
                    function (err,results) {
                      if (err) {
                        throw err;
                      }
                      console.log(results)
*/

        //console.log(results[0], typeof results[0], JSON.parse(results[0]))
                    //const cords = JSON.parse(results[0]);
                    //planet.x = cords.x;
                    //planet.y = cords.y;
                    //planet.z = cords.z;
                    //console.log(planet);
                    //planet.save();

                    }
                  )
    .catch((err) => console.log("Error: "+ err))
    console.log("doneZo")
  });



router.route('/addBulk').post(function(req, res) {
  console.log('hit');
  Planet.collection.insertMany(req.body)
  .then(() => console.log("added"))
  .catch((err) => console.log("Error: " + err));
  /*return new Promise(function(resolve,reject) {
    let len = req.body.length;
    for (let i = 0; i < len; i++) {
      console.log(i)
      let newPlanet = new Planet({
        pl_hostname : req.body[i].pl_hostname,
        pl_letter : req.body[i].pl_letter,
        st_dist : req.body[i].st_dist,
        dec : req.body[i].dec,
        ra_str : req.body[i].ra_str
      })
      newPlanet.save()
    }
  resolve();
});*/


  //newPlanets.save()
  //  .then(() => res.json('Planet added'))
  //  .catch(err => res.status(400).json('Error: ' + err));
  ////}
});

router.route('/deleteTop').delete((req,res) => {
  Planet.deleteOne()
  .then(() => res.json('Planet Top deleted.'))
  .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/deleteAll').delete((req,res) => {
  Planet.deleteMany()
  .then(() => res.json('Deleted All Planets'))
  .catch(err => res.status(400).json('Error: ' + err));
});
/*
router.route('/bulkAdd').post((req, res) => {
  const arrayLength = req.body.length;
  for (let i = 0; i < arrayLength; i++) {
    const newPlanet = new Planet({
      pl_hostname : req.body[i].pl_hostname,
      pl_letter : req.body[i].pl_letter,
      st_dist : req.body[i].st_dist
    });
    newPlanet.save()
      .then(() => res.json('Planet added!'))
      .catch(err => res.status(400).json('Error: ' +  err));
  }
});*/



module.exports = router;
