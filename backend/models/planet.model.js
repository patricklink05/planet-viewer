const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const planetSchema = new Schema({
  pl_hostname: {
    type: String,
  },
  pl_letter: {
    type: String,
  },
  st_dist: {
    type: Number
  },
  ra_str: {
    type: String
  },
  dec: {
    type: Number
  },
  x: {
    type: Number
  },
  y: {
    type: Number
  },
  z: {
    type: Number
  },
  pl_discmethod: {
    type: String
  }
}, {
  timestamps: true,
});

const Planet = mongoose.model('Planet', planetSchema);

module.exports = Planet;
